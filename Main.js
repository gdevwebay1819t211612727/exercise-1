var RandArray = [1,5,2,9,4,5,1,20];
var SampleDegrees = 96;
var SampleNumber = 45;
var Point1 = [3,5];
var Point2 = [-4,-1];

function number1(array)
{
	var largestNumb = array[0];
	for(var i = 0;i<=array.length-1;i++)
	{
		if(array[i]>largestNumb)
		{
			largestNumb=array[i];
		}
	}
	return largestNumb;
}

function number2(array)
{
	var SumOfNumb = 0;
	for(var i = 0;i<=array.length-1;i++)
	{
		SumOfNumb+=array[i];
	}
	return SumOfNumb;
}

function number3(array)
{
	var SumOfNumb = 0;
	var AverageOfNumb = 0;
	for(var i = 0;i<=array.length-1;i++)
	{
		SumOfNumb+=array[i];
	}
	return SumOfNumb/array.length;
}

function number4(degrees)
{
	var pi = 3.14;
	var Radians = 0;
	Radians=degrees/180;
	Radians*=pi;
	return Radians;
}

function number5(number,min,max)
{
	var dump;
	if(min>max)
	{
		dump=max;
		max=min;
		min=dump;
	}
	if(number>max)
	{
		return max;
	}
	else if(number<min)
	{
		return min;
	}
	else
	{
		return number;
	}
}

function number6(point1,point2)
{
	var euclidDist = 0;
	var dump = 0;
	dump = (point1[0]-point2[0])*(point1[0]-point2[0]) +
	 (point1[1]-point2[1])*(point1[1]-point2[1]);
	euclidDist = Math.sqrt(dump);
	return euclidDist;
}

function number7(array)
{
	var dump = 0;
	for(var i = 0;i<=array.length-1;i++)
	{
		for(var x = 0;x<=array.length-1;x++)
		{
			if(array[i]>array[i+1])
			{
				dump = array[i];
				array[i] = array[i+1];
				array[i+1] = dump;
			}
	    }
	}
	return array;
}
console.log(RandArray);
console.log(number7(RandArray));